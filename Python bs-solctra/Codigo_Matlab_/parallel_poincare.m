function parallel_poincare(t)
%PARALLEL_POINCARE Summary of this function goes here
%   Detailed explanation goes here
            Beta=135;
            poincare_cell{t}=poincare_section(Beta,t); 
            surface=poincare_cell{t};
            
            file=sprintf('pruebita/Surface1_%d.txt',t);
            %save(file,'surface','-ASCII');
            
 
            A=surface(:,1);
            B=surface(:,2);
            C=surface(:,3);
            D=sqrt(A.^2+B.^2);
            p=getCurrentTask();
            
            fileID=fopen(file,'w'); 
            fprintf(fileID,'x,y,z,R\n');
            for i=1:size(A)
                fprintf(fileID,'%d,%d,%d,%d\n',A(i),B(i),C(i),D(i));
            end
            fclose(fileID);
end


