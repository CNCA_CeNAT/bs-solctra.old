# coding: utf-8
import os
import numpy as np
import json
import math
import time
import h5py

from mpi4py import MPI
from numpy import linalg as LA
start_time = time.time()

amode = MPI.MODE_WRONLY|MPI.MODE_CREATE
comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()

#Se acceden a las constantes necesarias desde un archivo JSON
with open('config.json') as json_data_file:
    data = json.load(json_data_file)


R = data['R']#Radio mayor
r = data['r']#Radio menor
angle = data["angle"]#Angulo al que se desea obtener el poincare


#Se definen los puntos del plano utilizando las ecuaciones parametricas del toroide (https://es.wikipedia.org/wiki/Toro_(geometría))
# O = [x, y, z]
# Con phi = angle(radianes)
# Con theta para: O = 0, P = pi medios, Q = 5(pi)/4
O=[(R+r*np.cos(0))*np.cos(angle*np.pi/180),(R+r*np.cos(0))*np.sin(angle*np.pi/180),r*np.sin(0)]

P=[(R+r*np.cos(np.pi/2))*np.cos(angle*np.pi/180),(R+r*np.cos(np.pi/2))*np.sin(angle*np.pi/180),r*np.sin(np.pi/2)]

Q=[(R+r*np.cos(np.pi*1.25))*np.cos(angle*np.pi/180),(R+r*np.cos(np.pi*1.25))*np.sin(angle*np.pi/180),r*np.sin(np.pi*1.25)]



#Se obtienen las ecuaciones del plano por medio del discriminante
Pv_1=(P[1]-Q[1])*(P[2]-O[2])-(P[1]-O[1])*(P[2]-Q[2])

Pv_2=-((P[0]-Q[0])*(P[2]-O[2])-(P[0]-O[0])*(P[2]-Q[2]))

Pv_3=(P[0]-Q[0])*(P[1]-O[1])-(P[0]-O[0])*(P[1]-Q[1])


plane_vector=[Pv_1, Pv_2, Pv_3]




#En esta funcion se encuentran la direccion de cada path
def direcciones():
	#path = os.getcwd() #Se obtiene la direccion en la que se esta trabajando
	arr = []
	arr1=[]
	#carp="/PruebaPoincare" #Carpeta en las que se tienen los archivos de path
	#direccion=path+carp #Se crea el string con la direccion completa donde se encuentran los archivos de path
	direccion='/home/elias/mpi4py_poincare/pruebaPathPequeno'
	for files in next(os.walk(direccion))[2]:
		arr1.append(files)
		arr.append(direccion+"/"+files) #Se crea un vector con las direcciones con cada uno de los archivos
                
	tam=0
        numfiles = len(arr1)
        filearray = []
        namearray = []

        #if rank == 3:
        #    for x in range(rank*numfiles//4, numfiles -1):
        #        filearray.append(arr1[x])
         #       namearray.append(arr[x])
        #else:
        for x in range(rank*numfiles//4, (rank+1)*numfiles//4):
            filearray.append(arr1[x])
            namearray.append(arr[x])
	for files in namearray:
		lectura(files,filearray[tam])
		tam+=1
        #MPI.Resquest.Waitall()

        print("--- %s seconds ---" % (time.time() - start_time), rank)


#En esta funcion se encarga de leer los archivos de trayectoria
def lectura(files,nombre):
	path = np.loadtxt(files) #Se abren los archivos de paths como arrays de numpy
	cartersiana2toroidal(path,nombre)


def cart2torauxiliar(path, index, turn):
    	phi = 0
	if path[index][1] < 0:
		# 'a' es el punto (x,y) de circulo r
		a=[path[index][0],path[index][1]]
		# Se calcula la magnitud de a con LA.norm, luego con el el triángulo formado se aplica pitágoras,
		# finalmente se obtiene el complemento del ángulo phi(radianes) usado en las ecuaciones paramétricas.
		phi = np.arccos(path[index][0]/LA.norm(a))
		# Sacamos el phi que ocupamos, lo pasámos a un ángulo principal y se le suma el número de vueltas que lleva en
		# el toroiode y se representa como vuetas en polar.
		phi = (2*np.pi-phi)+2*np.pi*turn
	else:
		# Al estar la coordenada en y positiva el ángulo no es el complemento, sino, el ángulo que ocupamos
		# Por lo demás se apllica el mismo procedimiento
		a=[path[index][0],path[index][1]]
		phi = np.arccos(path[index][0]/LA.norm(a))
		phi = phi+2*np.pi*turn
	return phi

#En esta funcion se realiza la conversion cartersiana2toroidal pero unicamente para el phi,
#siguiendo la metodologia de Esteban Zamora
def cartersiana2toroidal(path,nombre):
	print ("Converting coordinates")
	turn=0
	V_phi=[]
	#filenew = open(os.getcwd()+'/PruebaPoincare/V_phi.txt', 'w')
	for j in range(1,len(path)-1):
		phi = cart2torauxiliar(path, j, turn)
		#filenew.write("V_phi[i] "+str(phi)+'\n')
		V_phi.append(phi)
		if path[j-1][1]<0 and path[j][1]>0:
			turn += 1
	#filenew.close()
	poincare(V_phi,path,nombre,turn)



#En esta funcion se hace toda la logica para crear los mapas de poincare
def poincare(V_phi,path,nombre,turn):
	print ("Calculating poincare section")
	#V_poincare = []
	print("turn ",turn)
	cont=0
	#filenew = open(os.getcwd()+'/PruebaPoincare/Angles.txt', 'w')
        turns = []
        n = 0

        #if rank == 3:
        #    for x in range(rank*turn//4, (rank+1)*turn//4):
        #        turns.insert(n, x)
         #       n+=1
        #else:
         #   for x in range(rank*turn//4, (rank+1)*turn//4 -1):
          #      turns.insert(n, x)
           #     n+=1


        #print "Turns: ", turns

        V_poincare = []
        for l in range (0, turn-1):
	#for l in turns:
		for i in range(1,len(V_phi)): #Se evalua en todos los angulos phi
			if V_phi[i]==((angle*np.pi/180)+(2*np.pi*l)):
				print('path:'+str(i))#Este if es solo para aquellos casos donde el phi es el mismo
				#Se appendea el punto al vector de poincare
				V_poincare.append(path[i])

		#Este aplica cuando el phi no es exactamente el mismo
		#print(str(V_phi[i-1]),str((angle*np.pi/180)+(2*np.pi*l)),str(V_phi[i]),str((angle*np.pi/180)+(2*np.pi*l)))
			if (V_phi[i]<((angle*np.pi/180)+(2*np.pi*l))) and (V_phi[i+1]>((angle*np.pi/180)+(2*np.pi*l))):
				#if V_phi[i-1]<((angle*np.pi/180)+(2*np.pi*l)) and V_phi[i]>((angle*np.pi/180)+(2*np.pi*l)):
					#Beta(k)<((angle*pi/180)+(2*pi*l))&Beta(k+1)>((angle*pi/180)+(2*pi*l))
				cont += 1
				print(cont)
				#filenew.write("V_phi[i] "+str(V_phi[i])+', Angle:  '+str((angle*np.pi/180)+(2*np.pi*l))+', V_phi[i+1] '+str(V_phi[i+1])+'\n')
				#Se crea la recta
				line_vector=path[i+1]-path[i]
				#Se hace el calculo de la interseccion entre el plano y la recta
				x=path[i][0]+((np.dot(plane_vector,P)-(plane_vector[0]*path[i][0]+plane_vector[1]*path[i][1]+plane_vector[2]*path[i][2]))/(plane_vector[0]*line_vector[0]+plane_vector[1]*line_vector[1]+plane_vector[2]*line_vector[2])*line_vector[0])
				y=path[i][1]+((np.dot(plane_vector,P)-(plane_vector[0]*path[i][0]+plane_vector[1]*path[i][1]+plane_vector[2]*path[i][2]))/(plane_vector[0]*line_vector[0]+plane_vector[1]*line_vector[1]+plane_vector[2]*line_vector[2])*line_vector[1])
				z=path[i][2]+((np.dot(plane_vector,P)-(plane_vector[0]*path[i][0]+plane_vector[1]*path[i][1]+plane_vector[2]*path[i][2]))/(plane_vector[0]*line_vector[0]+plane_vector[1]*line_vector[1]+plane_vector[2]*line_vector[2])*line_vector[2])
				#Se appendea el punto al vector de poincare
				V_poincare.append([x,y,z])
				break
			else:
				continue
	#filenew.close()
	escribir(V_poincare,nombre)


#Esta funcion es solo para escribir los archivos de superficies
def escribir(V_poincare,nombre):
	headers='x, y, z'
	filenew = open(os.getcwd()+'/PruebaPoincare/'+nombre, 'w')
	#filenew = h5py.File(os.getcwd()+'/PruebaPoincare/'+nombre, "w")
	#filenew = MPI.File.Open(comm, os.getcwd()+'/PruebaPoincare/'+nombre, amode)

	print(os.getcwd()+'/PruebaPoincare/'+nombre)
	filenew.write(headers + '\n')

	for i in range(0,len(V_poincare)):
		filenew.write(str(V_poincare[i][0])+', '+str(V_poincare[i][1])+', '+str(V_poincare[i][2])+'\n')
	filenew.close()


direcciones()
