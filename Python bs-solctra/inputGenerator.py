#coding=utf-8
import os
import numpy as np
import math
import sys
from numpy import linalg as LA

vector=[]
R = 0.2381 #Radio mayor
r = 0.0944165 #Radio menor
angle = int(sys.argv[1]) #Angulo al que se desea obtener el poincare
numPoints = int(sys.argv[2]) #Número de puntos a generar en la línea
width = (r*2)/numPoints #Distancia entre punto y punto
theta = angle*np.pi/180 

for i in range(1,numPoints):
    #El cálculo de la magnitud por cada punto
    magR = ((R-r)+i*width)
    #x,y,z ecuaciones para métricas para calcular la magnitud en cada eje
    O = [ magR * np.cos(theta), magR * np.sin(theta) , 0]
    vector.append(O)

#headers='x, y, z'
filenew = open('Path_Linea_Ang_'+str(angle)+'_Pts_'+str(numPoints)+'.txt', 'w')
#filenew.write(headers + '\n')

for i in range(0,len(vector)):
	filenew.write(str(vector[i][0])+'\t'+str(vector[i][1])+'\t'+str(vector[i][2])+'\n')
filenew.close()
