import os
import numpy as np
import math
from numpy import linalg as LA

vector=[]
num_points = 10
R = 0.2381 #Radio mayor
r = 0.0944165 #Radio menor
angle = 0 #Angulo al que se desea obtener el poincare
width = (r)/num_points
radians = (angle*np.pi)/180


for j in range(0, 360):
    for i in range(0, num_points):
        x = (R+(width*i)*np.sin(j*np.pi/180))*np.cos(radians)
        y = ((width*i)*np.cos(j*np.pi/180))
        z = (R+(width*i)*np.sin(j*np.pi/180))*np.sin(radians)
        O= [x, y, z]
        vector.append(O)


headers='x, y, z'
filenew = open('Path_Linea10.txt', 'w')
filenew.write(headers + '\n')

for i in range(0,len(vector)):
	filenew.write(str(vector[i][0])+', '+str(vector[i][1])+', '+str(vector[i][2])+'\n')
filenew.close()
